FROM ruby:2.6.5

RUN apt-get update -qq
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

ENV app /app
RUN mkdir $app
WORKDIR $app

ENV BUNDLE_PATH /box

COPY Gemfile /app/Gemfile

COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler:2.1.4
RUN bundle install
ADD . $app