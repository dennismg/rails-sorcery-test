class ApplicationController < ActionController::API
  include Sorcery::Controller
  include ActionController::HttpAuthentication::Token
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ActionController::RequestForgeryProtection

  include Pundit


  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # before_action :require_login

  # before_action :get_location
  # #
  # def get_location
  #   GeoHelper.get_location_data(request)
  # end

  def index
    redirect_to 'http://docs.gominno.com'
  end

  def current_user
    token = token_and_options(request).first if token_and_options(request).present?
    device = DeviceAuth.find_by(auth_token: token) if token.present?
    @current_user ||= User.find(device.user_id) if device.present?
    @current_user ||= User.find_by(auth_token: token)
  end

  protected

  def authenticate
    current_user || render_unauthorized
  end

  # def authenticate_token
  #   user = nil
  #   authenticate_with_http_token do |token, _options|
  #     user ||= User.find_by(auth_token: token)
  #     device = DeviceAuth.find_by(auth_token: token) unless user.present?
  #     user ||= User.find(device.user_id) if device.present?
  #   end
  # end

  def render_unauthorized
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render json: { errors: 'Bad credentials' }, status: 401
  end

  def not_authenticated
    render json: { errors: 'You must be logged in to perform that action!' }, status: 401
  end

  def pundit_user
    raise Pundit::NotAuthorizedError unless current_user.present?
    current_user
  end

  def find_user(login)
    if login.is_number?
      user = User.find(login)
    else
      if login.include? '@'
        user = User.all.where("lower(email) = lower(?)", login).first
      else
        user = User.all.where("lower(username) = lower(?)", login).first
      end
    end
  end

  def log_event(user, event_type, note)
    e = Event.new(user_id: user, event_type_id: event_type, note: note)
    e.save
  end

  def deprecated(old_method, replacement_method, year, month)
    warn "[DEPRECATION] The method: #{old_method} has been deprecated, please use #{replacement_method}. #{old_method} will no longer be accessible after #{year}-#{month}"
  end

  def bool(value)
    value.present? ? ([1, true, '1', 'true', 'yes', 't', 'y'].include?(value.downcase) ? true : false) : false
  end

  private

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    render json: { errors: (I18n.t "#{policy_name}.#{exception.query}", scope: 'pundit', default: :default) }, status: 401
  end
end
