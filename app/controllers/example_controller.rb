class ExampleController < ApplicationController
  def index
    render json: { "message": ENV['DATABASE_URL']}
  end

  def list
    render json: { "message": "LIST!"}
  end
end
