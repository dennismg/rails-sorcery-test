class SessionsController < ApplicationController
  # skip_before_filter :require_login
  # before_action :authenticate, except: :create

  def create
    user = find_user(params[:email])
    if user.present?
      un = user.username || user.email
      if user = login(un, params[:password])
        # current_user = user
        log_event(user.id, 4, nil)
        user.update_attribute(:gtm_id, params[:gtm_id]) unless user&.gtm_id&.present? && !params[:gtm_id].present?

        if params[:device_id].present?
          device = find_or_create_device(user)
        else
          user.update_attribute(:auth_token, user.generate_authentication_token!)
        end
        user_result = user.as_json(
        only: [:id, :email, :first_name, :last_name, :subscription_source, :created_at, :updated_at, :auth_token, :role ],
        methods: [:kids]
        )

        user_result[:auth_token] = device.auth_token if device.present?

        if user.admin?
          # NOTE: List all sections
          user_result[:security] = Section.all.as_json(only: [:name])
          # NOTE: Check access and role for each section
          user_result[:security].each do |section|
            section[:has_access] = user.has_section_access?(section['name'])
            sect = user.sections.where('sections.name = ?', section['name']).first
            section[:role] = sect.roles.first.name if sect.present?
          end
        end

        GoogleAnalyticsHelper.event(
          category: 'Account',
          action: 'User Activity',
          label: 'Login',
          client_id: (current_user ? current_user.gtm_id : nil),
          source: (current_user ? current_user.subscription_source : nil),
          user_id: (current_user ? current_user.id : nil),
          role: (current_user ? current_user.role : nil)
        )

        render json: user_result, status: 200
      else
        render json: { errors: 'Invalid email or password' }, status: 422
      end
    else
      render json: { errors: 'User not found' }, status: 422
    end
  end

  def destroy
    if current_user.present?
      current_user.update_attribute(:auth_token, current_user.generate_authentication_token!)
      GoogleAnalyticsHelper.event(
        category: 'Account',
        action: 'User Activity',
        label: 'Log Out',
        client_id: (current_user ? current_user.gtm_id : nil),
        source: (current_user ? current_user.subscription_source : nil),
        user_id: (current_user ? current_user.id : nil),
        role: (current_user ? current_user.role : nil)
      )
      logout
      head 204
    else
      render json: { errors: 'User not found by token' }, status: 422
    end
  end

  private

  def find_or_create_device(user)
    device = user.device_auths.find_by(device_id: params[:device_id])
    if device && device.active?
      device.update_attribute(:auth_token, user.generate_authentication_token!)
    else
      # if no device found create a new one
      device = user.device_auths.create(
        device_type: params[:device_type],
        device_id: params[:device_id],
        auth_token: user.generate_authentication_token!,
        active: true
      )
      log_event(user.id, 9, nil)
    end
    device
  end
end
