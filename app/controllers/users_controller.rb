class UsersController < ApplicationController
  # skip_before_filter :require_login
  # before_action :authenticate, except: :create
  # after_action :verify_authorized, except: :create
  # respond_to :json

  def index
    authorize User
    page = params[:page].present? ? params[:page] : 1
    items_per_page = params[:per_page].present? ? params[:per_page] : 10
    order = params[:order].present? ? params[:order] : 'id'
    direction = params[:dir].present? ? params[:dir] : 'asc'
    order_by_clause = "#{order} #{direction}"
    user_to_find = '1=1'
    user_to_find << " and users.id = #{params[:id]}" if params[:id].present?
    user_to_find << " and users.email like lower('%#{params[:email]}%')" if params[:email].present?
    # user_to_find ||= "lower(users.first_name || users.last_name) like '%lower(#{params[:name].gsub(' ', '')})%'" if params[:name].present?
    user_to_find << " and lower(users.first_name) LIKE '%#{params[:first_name].downcase.gsub(' ', '')}%'" if params[:first_name].present?
    user_to_find << " and lower(users.last_name) LIKE '%#{params[:last_name].downcase.gsub(' ', '')}%'" if params[:last_name].present?
    role_to_find = user_role_params if params['role'].present?
    # NOTE: uncomment to see sql output
    # p User.paginate(page: page, per_page: items_per_page).where(user_to_find).to_sql
    users = User.paginate(page: page, per_page: items_per_page)
            .where(user_to_find)

    users = users.where("users.role in (#{role_to_find})") unless role_to_find.nil?

    users = users.order(order_by_clause)
    render json: users.as_json(only: [:id, :first_name, :last_name, :email, :role, :subscription_source]) , status: 200

  end

  def show
    user = find_user(params[:id])
    if user.present?
      authorize user

      user_result = user.as_json(
      only: [:id, :email, :first_name, :last_name, :subscription_source, :subscriber_account_id, :created_at, :updated_at, :auth_token, :role ],
      methods: [:addresses, :phones, :kids, :devices, :billing_info, :subscription_info, :security, :tags]
      )
      if current_user.admin?
        user_result[:charges] = StripeHelper::Charge.retrieve_all(customer: user.subscriber_account_id) if %w(web android).include? user.subscription_source&.downcase
      begin
        user_result[:prorated_refund] = StripeHelper::Refund.get_proration(
                                          customer_id: user.subscriber_account_id,
                                          subscription_id: user.subscription_info.first['id']
                                  ) if %w(web android).include?(user.subscription_source&.downcase) && user.subscription_info.present?
      rescue
        render json: { errors: ['No upcoming invoices for customer'] }, status: 404 and return
      end

      end

      # NOTE: Hiding coupon data until needed
      # coupon_data = []
      # user.coupons.each do |coupon|
      #   coopun = coupon.attributes
      #   uc = user.coupon_users.where(coupon_id: coupon.id).first
      #   coopun[:processed] = uc.processed
      #   coopun[:process_date] = uc.process_date
      #   coupon_data << coopun
      # end if user.coupons.present?
      # user_result[:coupons] = coupon_data

      # NOTE: Hiding referral data until needed
      # referral_info = nil
      # referral_info = FriendBuyHelper::Customers.find(user.email)
      # referral_info[:shares] = FriendBuyHelper::Customers.shares(user.email)
      # referral_info[:referral_codes] = FriendBuyHelper::Customers.referral_codes(user.email)
      # referral_info[:conversions] = FriendBuyHelper::Customers.conversions(user.email)
      # referral_info[:email_recipients] = FriendBuyHelper::Customers.email_recipients(user.email)
      # referral_info[:rewards] = FriendBuyHelper::Customers.rewards(user.email)
      # user_result[:referrals] = referral_info

      render json: user_result, status: 200 # if current_user.admin? || current_user == user
    else
      render json: { errors: ['User not found'] }, status: 404
    end
  end

  def create
    begin
      user = User.new(user_params)
    rescue ActionController::ParameterMissing
     render json: { errors: { missing_hash: ['The user hash must be set to update']} }, status: 400 and return
    end

    if user.save
      log_event(user.id, 1, nil)
      GoogleAnalyticsHelper.event(
        category: 'Pricing Funnel',
        action: 'Step 2 - Account Create',
        label: (user ? user.gtm_id : nil),
        client_id: (user ? user.gtm_id : nil),
        source: (user ? user.subscription_source : nil),
        user_id: (user ? user.id : nil),
        role: (user ? user.role : nil)
      )
      user_result = user.as_json(
      only: [:id, :email, :first_name, :last_name, :subscription_source, :subscriber_account_id, :created_at, :auth_token, :role, :gtm_id ]
      )
      # Clear cached data immediately
      user.update_attribute(:billing_data, nil)
      user.update_attribute(:subscription_data, nil)
      render json: user_result, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def update
    user = find_user(params['id'])
    user_attr_before_update = user.attributes
    authorize user

    begin
      user.email = user_params[:email] if user_params[:email].present?
      user.password = user_params[:password] if user_params[:password].present?
      user.first_name = user_params[:first_name] if user_params[:first_name].present?
      user.last_name = user_params[:last_name] if user_params[:last_name].present?
      user.username = user_params[:username] if user_params[:username].present?
      user.role = user_params[:role] if user_params[:role].present?
      user.subscriber_account_id = user_params[:subscriber_account_id] if user_params[:subscriber_account_id].present?
      user.subscription_source = user_params[:subscription_source] if user_params[:subscription_source].present?

      if user.update(user_params.reject{|_, v| v.blank?})
        begin
          # intercom_user = # IntercomHelper.find_user( user.id )
          # IntercomHelper.update_customer_attrib( intercom_user.id, 'ROLE', user.role )
          # IntercomHelper.update_customer_attrib( intercom_user.id, 'EMAIL', user.email )
          # IntercomHelper.update_customer_attrib( intercom_user.id, 'FIRST_NAME', user.first_name )
          # IntercomHelper.update_customer_attrib( intercom_user.id, 'LAST_NAME', user.last_name )
          # IntercomHelper.update_customer_attrib( intercom_user.id, 'SUBSCRIPTION_SOURCE', user.subscription_source )
        rescue
          log_event(user.id, 12, 'Unable to update intercom info')
        end
        user_attr_after_update = user.attributes
        log_event(user.id, 2, "#{user_attr_before_update} >>> #{user_attr_after_update}")

        user_result = user.as_json(
        only: [:id, :email, :first_name, :last_name, :subscription_source, :subscriber_account_id, :created_at, :auth_token, :role, :gtm_id ]
        )
        # Clear cached data immediately
        user.update_attribute(:billing_data, nil)
        user.update_attribute(:subscription_data, nil)
        render json: user_result, status: 200
      else
        render json: { errors: user.errors }, status: 422
      end
    rescue ActionController::ParameterMissing
     render json: { errors: { missing_hash: ['The user hash must be set to update']} }, status: 400 and return
    end
  end

  def destroy
    user = find_user(params['id'])
    if user == current_user
      log_event(user.id, 3, nil)
      authorize user
      user.update_attribute(:role, 0)
      user.update_attribute(:status, 'Canceled')
      user.update_attribute(:auth_token, user.generate_authentication_token!)
      # Clear cached data immediately
      user.update_attribute(:billing_data, nil)
      user.update_attribute(:subscription_data, nil)
      begin
        # intercom_user = # IntercomHelper.find_user( user.id )
        # IntercomHelper.update_customer_attrib( intercom_user.id, 'ROLE', 'user' )
        # IntercomHelper.update_customer_attrib( intercom_user.id, 'STATUS', 'Canceled' )
      rescue
        log_event(user.id, 12, 'Unable to update intercom info')
      end
      head 204
    end
  end

  def set_active_episode
    user = find_user(params[:user_id])
    authorize user
    if current_user.update_attribute(:active_episode, params['embed_code'])
      # render json: user, status: 200, location: [:api, user]
      head 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def tag_in_intercom
    authorize current_user
    # NOTE: this method exists so the website can send tags independent of other actions
    render json: { errors: ['User not found'] }, status: 404 and return unless current_user.present?
    # IntercomHelper.tag_user(current_user, params[:value]) if params[:value].present?
    head 200
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :username, :role, :subscriber_account_id, :subscription_source, :gtm_id)
  end

  def user_role_params
    all_roles = params['role'].split(',')
    permitted_role_values = []
    all_roles.each do |role|
      if %w(user subscriber vip admin temp paypal).include?(role)
        permitted_role_values.push(User.roles[role])
      end
    end
    permitted_role_values.join(',')
  end
end
