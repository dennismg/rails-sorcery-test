module EmailHelper

  def self.email_header(preview_text: nil)
    "
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
      <!--[if !mso]><!-->
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
      <!--<![endif]-->
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
      <title></title>
      <!-- Base CSS Styles DO NOT EDIT THESE -->
      <style type=\"text/css\">
        body {
          margin: 0 !important;
          padding: 0;
          background-color: #ffffff;
        }
        table {
          border-spacing: 0;
          font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;
          font-size: 16px;
          font-weight: 400;
          text-rendering: optimizeLegibility;
          -webkit-font-smoothing: antialiased;
          line-height: 24px;
          color: #5a6066;
        }
        td {
          padding: 0;
        }
        img {
          border: 0;
        }
        div[style*=\"margin: 16px 0\"] {
          margin: 0 !important;
        }
        .wrapper {
          width: 100%;
          table-layout: fixed;
          -webkit-text-size-adjust: 100%;
          -ms-text-size-adjust: 100%;
          background-color: #f2f2f2;
        }
        .webkit {
          max-width: 600px;
          margin: 0 auto;
        }
        .outer {
          Margin: 0 auto;
          width: 100%;
          max-width: 600px;
          background: #ffffff;
        }
        .inner {
          padding: 0 2%;
        }
        .full-width-image img {
          width: 100%;
          max-width: 600px;
          height: auto;
          display: block;
        }
        .copy-block {
          margin-top: 40px;
          margin-bottom: 20px;
          padding: 0 3%;
        }
        .header {
          background-color: #f2f2f2;
        }
        .title {
          padding: 0 4%;
        }
        .title img {
          margin-bottom: 20px;
        }
        .contents {
          margin-bottom: 20px;
        }
        p {
          Margin: 0;
        }
        a {
          color: #ee6a56;
          text-decoration: underline;
        }
        .footer {
          background-color: #f2f2f2;
        }
        /*****************DO NOT EDIT! MUST BE HERE FOR GMAIL APPS*********************/
        @media screen and (max-width: 400px) {
          .body {
            min-width: 100vw;
            outline: 20px solid #f2f2f2;
          }
        }
        /*******************DO NOT EDIT! MUST BE HERE FOR GMAIL APPS*******************/
        /************************************************************/
        /* One column layout */
        /************************************************************/
        .one-column .contents {
          text-align: left;
        }
        .first-paragraph {
          margin-top: 40px;
          margin-bottom: 0px;
        }
        .one-column p {
          Margin-bottom: 20px;
        }
        /************************************************************/
        /*Two column layout*/
        /************************************************************/
        .two-column {
          text-align: center;
          padding: 0px 4%;
          padding-bottom: 20px;
        }
        .two-column .column {
          width: 100%;
          max-width: 273px;
          display: inline-block;
          vertical-align: top;
        }
        .contents {
          width: 100%;
        }
        .two-column .contents {
          text-align: left;
        }
        .two-column img {
          width: 100%;
          max-width: 280px;
          height: auto;
        }
        .two-column .text {
          padding-top: 10px;
          text-align: center;
        }
        /************************************************************/
        /*Three column layout*/
        /************************************************************/
        .three-column {
          text-align: center;
          padding: 0px 4%;
          padding-bottom: 20px;
        }
        .three-column .column {
          width: 100%;
          max-width: 180px;
          display: inline-block;
          vertical-align: top;
        }
        .three-column .contents {
          text-align: center;
        }
        .three-column img {
          width: 100%;
          max-width: 191px;
          height: auto;
        }
        .three-column .text {
          padding-top: 10px;
        }
        /************************************************************/
        /*Four column layout*/
        /************************************************************/
        .four-column {
          width: 100%;
          text-align: center;
          padding: 0px 4%;
          margin-bottom: 20px;
        }
        .four-column .column {
          width: 100%;
          max-width: 134px;
          display: inline-block;
          vertical-align: top;
        }
        .four-column .contents {
          text-align: center;
        }
        .four-column img {
          width: 100%;
          max-width: 150px;
          height: auto;
          margin-bottom: 16px;
        }
        .app-badges {
          padding: 0 5px;
        }
        @media screen and (max-width: 400px) {
          .app-badges {
            padding: 0px;
          }
        }
      </style>
    <!-- End Styles -->
    </head>
    <body class=\"body\">
      <center class=\"wrapper\">
        <div class=\"webkit\">
          <!--[if (gte mso 9)|(IE)]>
    <table width=\"600\" align=\"center\">
    <tr>
    <td>
    <![endif]-->
    <!-- Visually Hidden Preheader Text : BEGIN -->
    <!-- Change this text to show as a preview in most email clients below the subject -->
    <div style=\"display:none;font-size:1px;color:#222222;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide: all;\">
    #{preview_text}
    </div>
    <!-- Visually Hidden Preheader Text : END -->
    <table class=\"outer\" align=\"center\">
      <!-- **************************************************************
      Header Image
      ************************************************************** -->
      <tr>
        <td class=\"full-width-image header\">
          <img src=\"https://s3.amazonaws.com/jt-v3-assets/email-assets/Header.jpg\">
        </td>
      </tr>
      <!-- End Header Image -->
      <!-- **************************************************************
      One Column - Best for full width text
      ************************************************************** -->
      <tr>
        <td class=\"one-column\">
          <table class=\"copy-block\" width=\"100%\">
            <tr>
              <td class=\"inner contents\">
    "
  end

  def self.email_footer(fine_print: nil, campaign: nil, campaign_content: nil)
    "
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <!-- End One Column -->
    <!-- **************************************************************
    Fine Print
    ************************************************************** -->
    " +
    if fine_print.present?
    "
    <tr>
    <td class=\"one-column\">
    <table width=\"100%\">
    <tr>
    <td style=\"padding: 4%; text-align:center; font-size:10px; color: #979797; line-height:14px;\">
      #{fine_print}
    </td>
    </tr>
    </table>
    </td>
    </tr>
    "
    else ''
    end + "
    <!-- End Fine Print -->
    <!-- **************************************************************
    Footer
    ************************************************************** -->
    <tr>
    <td class=\"one-column footer\">
    <table width=\"100%\">
    <tr>
    <tr>
      <td valign=\"middle\" width=\"80\" style=\"padding: 16px 0; text-align:center; line-height: 0;\" class=\"stack-column-center\">
        <a href=\"https://www.gominno.com?utm_source=application&utm_medium=email&utm_campaign=#{campaign}&utm_content=#{campaign_content}\">
                          <img src=\"https://jt-v3-assets.s3.amazonaws.com/email-assets/minno+logo+emails.png\" alt=\"Minno Kids\" width=\"80\" style=\"margin: auto;\">
                      </a>
      </td>
    </tr>
    <tr>
      <td style=\"text-align:center; padding: 0 0 12px 0; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: 400; text-rendering: optimizeLegibility; -webkit-font-smoothing: antialiased;line-height: 26px; color: #5a6066;\">
        <span class=\"mobile-link--footer\">818 18th Ave S, Floor 10, Nashville, TN 37203</span>
        <br>
      </td>
    </tr>
    <tr>
      <td style=\"text-align:center; padding: 0 0 16px 0; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; text-rendering: optimizeLegibility; -webkit-font-smoothing: antialiased;line-height: 26px; color: #5a6066;\">
        <a class=\"mobile-link--footer\" href=\"https://www.facebook.com/minno\" style=\"padding: 0 5px\"><img src=\"https://res.cloudinary.com/ctv/image/upload/gutter-facebook.png\" alt=\"Facebook\" height=\"18\" style=\"margin: auto;\"></a>
        <a class=\"mobile-link--footer\" href=\"https://twitter.com/go_minno\" style=\"padding: 0 5px\"><img src=\"https://res.cloudinary.com/ctv/image/upload/gutter-twitter.png\" alt=\"Twitter\" height=\"18\" style=\"margin: auto;\"></a>
        <a class=\"mobile-link--footer\" href=\"https://instagram.com/gominno\" style=\"padding: 0 5px\"><img src=\"https://res.cloudinary.com/ctv/image/upload/gutter-instagram.png\" alt=\"Instagram\" height=\"18\" style=\"margin: auto;\"></a>
        <a class=\"mobile-link--footer\" href=\"https://pinterest.com/gominno\" style=\"padding: 0 5px\"><img src=\"https://res.cloudinary.com/ctv/image/upload/gutter-pinterest.png\" alt=\"Pinterest\" height=\"18\" style=\"margin: auto;\"></a>
      </td>
    </tr>
    </tr>
    </table>
    </td>
    </tr>
    <!-- End Footer -->
    </table>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
    </div>
    </center>
    </body>
    </html>
    "
  end


  def self.app_badge_row
    "<table width=\"100%\">
    <tr>
      <td style=\"text-align:center; padding: 0 0 16px 0; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; text-rendering: optimizeLegibility; -webkit-font-smoothing: antialiased;line-height: 26px; color: #5a6066;\">
        <a class=\"app-badges\" href=\"https://apps.apple.com/au/app/minno-kids/id705286113\" target=\"_blank\"><img src=\"https://res.cloudinary.com/ctv/image/upload/appstore-badge-updated.png\" alt=\"App Store\" height=\"40\" style=\"margin: auto;\"></a>
        <a class=\"app-badges\" href=\"https://play.google.com/store/apps/details?id=com.jellytelly&hl=en\" target=\"_blank\"><img src=\"https://res.cloudinary.com/ctv/image/upload/GooglePlay-Badge-Updated.png\" alt=\"Google Play\" height=\"40\" style=\"margin: auto;\"></a>
        <a class=\"app-badges\" href=\"https://www.amazon.com/gp/product/B017MVYM1C\" target=\"_blank\"><img src=\"https://res.cloudinary.com/ctv/image/upload/Amazon-Badge-Updated.png\" alt=\"Amazon Appstore\" height=\"40\" style=\"margin: auto;\"></a>
        <a class=\"app-badges\" href=\"https://my.roku.com/add/minnokids\" target=\"_blank\"><img src=\"https://res.cloudinary.com/ctv/image/upload/roku-badge.png\" alt=\"Roku\" height=\"40\" style=\"margin: auto;\"></a>
      </td>
    </tr>
    </table>
    "
  end

end
