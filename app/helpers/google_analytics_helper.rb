# frozen_string_literal: true

module GoogleAnalyticsHelper
  class << self
    def event(category:, action:, label:, client_id:, source: 'web', user_id: nil, role: nil)
      # NOTE: We are not sending video || content interaction events to GA for now as they cause the session daily hit rate to be maxed out
      return if (category&.downcase&.include?('video') || category&.downcase&.include?('content interaction'))

      ga_key = Rails.cache.fetch('ga_key') do
        Setting.find_by(key: 'ga_key')&.value
      end
      return unless ga_key.present?

      ga_version = Rails.cache.fetch('ga_version') do
        Setting.find_by(key: 'ga_version')&.value
      end
      # Default to v1
      ga_version ||= 1

      ga_endpoint = Rails.cache.fetch('ga_endpoint') do
        Setting.find_by(key: 'ga_endpoint')&.value
      end
      # Default
      ga_endpoint ||= 'http://www.google-analytics.com/collect'

      # These are required by GA
      return unless category.present? && action.present?

      client_id = (client_id || user_id || '555')
      source = 'web' unless source.present? && ['amazon', 'amazon tv', 'android', 'android tv', 'ios', 'tvos', 'roku', 'web', 'stripe'].to_set.include?(source.downcase)

      # Set params
      params = {
        v: ga_version,
        tid: ga_key,
        cid: client_id,
        t: 'event',
        ec: category,
        ea: action,
        el: label,
        dh: 'www.gominno.com'
      }
      params['cd1'] = client_id || '555'
      params['cd2'] = source&.downcase || 'web'
      params['cd3'] = ENV['ENVIRONMENT']
      params['cd4'] = user_id || 'N/A'
      params['cd5'] = role || 'N/A'
      params['cd6'] = Time.now
      begin
        p 'DEBUG: GoogleAnalyticsHelper Params'
        p params
        RestClient.get(ga_endpoint, params: params, timeout: 4, open_timeout: 4)
        return true
      rescue StandardError => e
        p e
        Rollbar.log('error', e)
        return false
      end
    end
    handle_asynchronously :event, priority: 10, queue: 'google_analytics'
  end
end
