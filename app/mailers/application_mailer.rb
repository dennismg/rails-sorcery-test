class ApplicationMailer < ActionMailer::Base
  default from: "admin@gominno.com"
  layout 'mailer'
end