# NOTE: to preview emails follow instructions in this post: https://richonrails.com/articles/action-mailer-previews-in-ruby-on-rails-4-1
# NOTE: preview url format: http://localhost:5000/rails/mailers/user_mailer/welcome_email

class UserMailer < ApplicationMailer
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper

  default from: 'support@gominno.com'

  def abandoned_signup(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: 'Begin your free week')
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>"
    body << "<p>It looks like you didn't finish signing up. Did you know <a href='https://www.gominno.com' target='_blank'>Minno Kids</a> is more than just videos? We built Minno Kids to help you as a parent jumpstart conversations about faith with your kids - through the shows they watch, the devotionals you can do together and other resources available on our website.</p>"
    body << "<p>Your kids will love having their own app and the ability to choose the shows they want to watch - and you'll feel great knowing that the shows are safe, faith-friendly, and ad-free!</p>"
    body << "<p>Other parents have said:<br>
    <em>\"My family has a subscription to Minno Kids, and we LOVE it!! In fact, it's almost exclusively what my 5 and 6 year old watch. They're really not much interested in Disney movies anymore, they'd much rather watch Friends and Heroes.\" - Rebecca T.</em>
    <br><br>
    <em>\"We love Minno Kids because it is a safe, educational, fun environment which gives me another amazing tool to teach our children from the Bible! Thank you!!!\"- Angela L.</em>"
    body << '<p>Finish signing up today to start your free week so your kids can start enjoying the great shows and devotionals on Minno Kids anytime, anywhere.</p>'

    body << "<center><a href='https://www.gominno.com/account#billing' target='_blank' style='text-alignment: center;'>"
    body << "<button style='height: 60px; width: 340px; background-color: #E68556; color: #fff; border-radius: 4px; font-weight: bold; font-size: 24px;'>Start Your 7 Day Free Trial</button>"
    body << '</a></center>'
    fine_print = "Reply to this email if you have any questions - we're happy to help!"
    body << EmailHelper.email_footer(fine_print: fine_print, campaign: 'abandoned_signup', campaign_content: 'begin_free_week')

    mail(to: @user.email, subject: 'Don\'t forget Minno Kids...', body: body, content_type: 'text/html')
  end

  def welcome_email(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: 'Welcome')
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>
    <p>We're excited to provide your family with shows and devotionals your kids will love that will also help them experience God's goodness throughout the week.</p>
    <p>Watch Minno Kids on your family's devices by downloading the apps from the links below:</p>"
    body << EmailHelper.app_badge_row
    body << "
    <p>If you have questions or need help at any point, we're here for you! Visit our <a href='https://minno.helpdocs.io/' target='_blank'>Help Center</a> to find answers to common questions or reach out to us at support@gominno.com.</p>
    <p>Thanks again for joining us, and we hope you enjoy Minno Kids!</p>"
    body << EmailHelper.email_footer(fine_print: '', campaign: 'welcome_series', campaign_content: 'welcome')

    mail(to: @user.email, subject: 'Welcome to Minno Kids', body: body, content_type: 'text/html')
  end

  def trial_ending(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: "How's it going?")
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>
    <p>We hope you have been enjoying <a href='https://www.gominno.com'>Minno Kids</a> so far! We wanted to remind you that your free trial ends in <strong>2 days</strong>, and you will be charged at that time.</p>
    <p>Please let us know if you have any questions or feedback at this point by replying to this email. We'd love to help!</p>
    <p>Thanks again for joining us, and we hope you enjoy Minno Kids!</p>"

    fine_print = '<p>Changed your mind about Minno Kids? <a href=\"https://www.gominno.com/login">Log into your account</a> to cancel before your free trial ends and you will not be charged.<br>Didn\'t sign up for Minno Kids? Please email us at support@gominno.com.</p>'

    body << EmailHelper.email_footer(fine_print: fine_print, campaign: 'welcome_series', campaign_content: 'trial_ending')

    mail(to: @user.email, subject: "How's it going?", body: body, content_type: 'text/html')
  end

  def first_day_as_member(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Membership')
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>
    <p>Thank you for becoming a Minno Kids member, and we hope you enjoyed your 7-day trial. We're excited to have you as part of the Minno Kids community. Here are a few things you need to know to make the most of your new subscription.</p>"
    body << '<ol>'
    body << '<li>You can update your email, password, payment methods, and subscription plan at any time from the website by logging into your Minno Kids account and going to "My Account" in the menu.</li>'
    body << "<li>If you haven't already, make sure you've downloaded the Minno Kids app to your family's devices. Did you know you can download shows to your mobile devices and also stream Minno Kids on your Apple TV, Roku, Amazon Fire TV and/or cast it via your Chromecast? Learn more <a href='http://gominno.com/apps'>here</a>.</li>"
    body << "<li>Use the social icons below to follow Minno Kids on Facebook, Twitter, Instagram, and Pinterest to stay up to date on what's new with Minno Kids and to connect with other Minno Kids members.</li>"
    body << '</ol>'

    fine_print = "<p>Need help? Check out the <a href='https://help.gominno.com'>Minno Kids Help Center</a> or email us at support@gominno.com, or simply reply to this email.</p>"

    body << EmailHelper.email_footer(fine_print: fine_print, campaign: 'welcome_series', campaign_content: 'first_day_as_member')

    mail(to: @user.email, subject: 'Minno Kids Membership', body: body, content_type: 'text/html')
  end

  def nps(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: 'Would you recommend us?')
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>
    <p>We hope your family has been enjoying Minno Kids! We have one quick question to ask you:</p>
    <p><center><strong>How likely are you to recommend Minno Kids to a friend?</strong></center></p>"
    body << "<center><a href='http://www.surveygizmo.com/s3/4121642/NPS-Survey' target='_blank' style='text-alignment: center;'>"
    # body << "<img src='http://res.cloudinary.com/ctv/image/upload/v1500396508/answer-question.png' />"
    body << "<button style='height: 50px; width: 150px; background-color: #39bbbb; color: #fff; border-radius: 4px; font-weight: bold;'>LET US KNOW</button>"
    body << '</a><br><em>It only takes a moment!</em></center>'
    body << EmailHelper.email_footer(fine_print: '', campaign: 'nps', campaign_content: 'nps')
    mail(to: @user.email, subject: 'Quick question from Minno Kids...', body: body, content_type: 'text/html')
  end

  def annual_renewal(user)
    @user = user

    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Renewal')
    body << "<h2>Hi #{@user.first_name || 'there'},</h2>
    <p>Thank you for being a Minno Kids member for almost a year! We hope you and your family are loving everything that Minno Kids has to offer.</p>
    <br>
    <p>Please note: your annual subscription is set to <strong>auto-renew in 15-days</strong>.</p>
    <br>
    <p>Your membership helps us continue providing the best Christian shows and devotionals for kids. We appreciate you!</p>"

    fine_print = "If you wish to cancel prior to your renewal, please follow <a href='https://minno.helpdocs.io/article/vm7fcs9ll5'>these instructions</a>."
    body << EmailHelper.email_footer(fine_print: fine_print, campaign: 'renewal_notice', campaign_content: 'renewal_notice')
    mail(to: @user.email, subject: 'Your Minno Kids Subscription renews soon', body: body, content_type: 'text/html')
  end

  def reset_password_email(user)
    @user = find_user(user.id)

    @env = nil

    @env = case ENV!['ENVIRONMENT']
           when 'dev'
             'dev'
           when 'test'
             'qa'
           when 'prod'
             'www'
           else
             'www'
           end

    @url = "https://#{@env}.gominno.com/password_resets/#{@user.reset_password_token}"
    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Password Reset Request')
    body << "<h2>Hello, #{@user.first_name}</h2>
      You have requested to reset your password.
      <br>
      To choose a new password, just follow this link: <a href='#{@url}'>Password Reset Link</a>.
      <br>
      If you feel this was sent in error, you can ignore this request as it will expire in 2 hours.
      <br>
      Have a great day!"
    body << EmailHelper.email_footer(fine_print: '')

    mail(to: @user.email, subject: 'Minno Kids password reset request', body: body, content_type: 'text/html')
  end

  def password_reset_confirmation(user)
    @user = find_user(user.id)

    @env = nil

    @env = case ENV!['ENVIRONMENT']
           when 'dev'
             'dev'
           when 'test'
             'qa'
           when 'prod'
             'www'
           else
             'www'
           end

    @url = "https://#{@env}.gominno.com/password/reset}"
    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Password Reset Confirmation')
    body << "<h2>Hello, #{@user.first_name}</h2>
      <p>
        <strong>Your Minno Kids password has been reset.</strong>
      </p>
      <p>
      If you feel this was sent in error, you did not make, or request this change please follow this <a href='#{@url}'>link</a> to choose a new password.
      </p>
      <p>
      If you are unable to access the password reset page or are not recieving the reset email, please contact us support@gominno.com, or you can simply reply to this email.
      </p>
      Have a great day!"
    body << EmailHelper.email_footer(fine_print: '')

    mail(to: @user.email, subject: 'Your Minno Kids password has been reset.', body: body, content_type: 'text/html')
  end

  def invalid_creditcard_email(user)
    @user = find_user(user.id)
    body = ''
    body << EmailHelper.email_header(preview_text: 'Invalid Credit Card Info')

    body << "<h2>Hello, #{@user.first_name}</h2>
      <br>
      Your credit card on file is no longer active, please update your credit card to continue receiving access to Minno Kids.
      <br>
      Just follow this <a href='https://www.gominno.com/login'>link</a> to login and update your info.
      <br>
      Have a great day!"
    body << EmailHelper.email_footer(fine_print: '')
    mail(to: @user.email, subject: 'Minno Kids Account Status', body: body, content_type: 'text/html')
  end

  def pending_cancellation_confirmation(user)
    @user = find_user(user.id)

    sub_info = @user.subscription_data.present? ? @user.subscription_data : @user.subscription_info

    cancellation_date = sub_info.present? ? Time.at(sub_info&.first['current_period_end'])&.strftime('%b %e, %Y') : nil

    first_name = @user.first_name || 'There'
    url = 'https://www.gominno.com/account#billing'
    todays_date = Date.today.to_s
    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Cancellation Pending')
    body << "<h2>Hello, #{first_name}</h2>
      We\'re really sorry to see you go, but this email confirms that your Minno Kids membership has been marked for cancellation as of #{todays_date}.
      <p>
      <strong>Your membership will be cancelled on #{cancellation_date}</strong>
    </p>
    <p>
      If you feel that this was in error please contact us at support@gominno.com or just reply to this email and well get the issue resolved.
    </p>
    <p>
      <strong><em>If you want to reactivate your membership just log in and go to your <a href=\"#{url}\">account</a> page and click the Reactivate Subscription link <span style=\" color: #ee6a56;\">before #{cancellation_date}</span></em></strong>.
    </p>
    <p>
      Have a great day!
    </p>"
    body << EmailHelper.email_footer(fine_print: 'Once the cancellation has been processed it is effective immediately, and any promotional plan that is no longer offered will not be available in the future.')

    mail(to: @user.email, subject: 'Minno Kids Cancellation Pending', body: body, content_type: 'text/html')
  end

  def cancellation_confirmation(user)
    @user = find_user(user.id)

    # NOTE: commented out for now, because we can't get customer cancel date
    # case @user.subscription_source
    #   when 'WITB PayPal'
    #     billing_info = {}
    #     subscription_info = []
    #   when  'Paypal'
    #     billing_info = PaypalHelper::Agreement.retrieve(user.subscriber_account_id) if user.subscriber_account_id.present?
    #     subscription_info = billing_info[:subscriptions][:data] if billing_info.present?
    #   when 'iOS'
    #     billing_info = user.subscription_source
    #     subscription_info = AppleHelper.validate_receipt(user.subscriber_account_id, user.id)
    #   else
    #     billing_info = StripeHelper::Customer.retrieve(user.subscriber_account_id) if user.subscriber_account_id.present?
    #     subscription_info = billing_info.respond_to?(:deleted) ? "Customer #{user.subscriber_account_id} Deleted" : StripeHelper::Subscription.retrieve_all(user.subscriber_account_id)
    # end

    first_name = @user.first_name || 'There'
    url = 'https://www.gominno.com/account#billing'
    todays_date = Date.today.to_s
    body = ''
    body << EmailHelper.email_header(preview_text: 'Minno Kids Cancellation Confirmation')
    body << "<h2>Hello, #{first_name}</h2>
      We\'re really sorry to see you go, but this email confirms that your Minno Kids membership has been canceled as of #{todays_date}.
    </p>
    <p>
      If you feel that this was in error please contact us at support@gominno.com or just reply to this email and well get the issue resolved.
    </p>
    <p>
      If you ever want to sign back up just log in and go to your <a href=\"#{url}\">account</a> page and choose a plan.
    </p>
    <p>
      Have a great day!
    </p>"
    body << EmailHelper.email_footer(fine_print: 'Cancellation is effective immediately, and any promotional plan that is no longer offered will not be available in the future.')

    mail(to: @user.email, subject: 'Minno Kids Cancellation Confirmation', body: body, content_type: 'text/html')
  end

  private

  def find_user(login)
    # If you edit method, you most likely need to edit the method in application controller
    if login.is_number?
      User.find(login)
    elsif login.include? '@'
      User.where('lower(email) = lower(?)', login).first
    else
      User.where('lower(username) = lower(?)', login).first
    end
  end
end
