class DeviceAuth < ApplicationRecord
    belongs_to :user, optional: true
  
    delegate :email, :role, to: :user, allow_nil: true, prefix: true
  end