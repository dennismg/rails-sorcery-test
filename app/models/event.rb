# frozen_string_literal: true

class Event < ApplicationRecord
  belongs_to :user
  has_one :event_type

  after_commit :update_sentiment, on: %i[create]

  def event_type
    et = EventType.find_by(id: event_type_id)
    et.description
  end

  private

  def update_sentiment
    # NOTE: only update sentiment for cancellations
    return unless event_type_id == 8
    return unless note&.downcase&.include?('comment')

    begin
      comment = note.downcase
      text = comment[comment.index('comment:') + 9, comment.length]

      aws_sentiment_resp = Aws::Comprehend::Client.new(region: 'us-east-1').detect_sentiment(
        text: text.byteslice(0...4999), # required
        language_code: 'en' # required, accepts en, es, fr, de, it, pt
      )
      return unless aws_sentiment_resp.present?

      update(sentiment: aws_sentiment_resp.sentiment) #=> String, one of "POSITIVE", "NEGATIVE", "NEUTRAL", "MIXED"
      # aws_sentiment_resp.sentiment_score.positive #=> Float
      # aws_sentiment_resp.sentiment_score.negative #=> Float
      # aws_sentiment_resp.sentiment_score.neutral #=> Float
      # aws_sentiment_resp.sentiment_score.mixed

      ActiveCampaignHelper.sync_user(user)
    rescue StandardError
    end
  end
end
