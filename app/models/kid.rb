class Kid < ApplicationRecord
  before_save :default_values

  validates :gender, inclusion: {in: ['m', 'f', 'u', '', nil]}
  validates :dob, date: { on_or_before: Time.zone.today}
  validates :pin, length: { minimum: 4, maximum: 6 }, if: Proc.new { |k| k.pin.present?}
  belongs_to :user

  private
  def default_values
    self.gender = 'u' unless self.gender.present?
  end

end
