# class User < ApplicationRecord
#   authenticates_with_sorcery!

#   validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
#   validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
#   validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }

#   validates :email, uniqueness: true
# end

# frozen_string_literal: true

class User < ApplicationRecord
  # Define roles
  enum role: %i[user subscriber vip admin temp paypal]

  # Set before/after filters, and filter methods
  before_create :generate_authentication_token!
  after_initialize :set_default_role, if: :new_record?
  before_update :check_password_length
  before_update :check_password_conf
  after_create :sync_to_active_campaign
  # after_update :sync_to_active_campaign

  def set_default_role
    self.role ||= :user
  end

  authenticates_with_sorcery!

  # Set validations
  validates :auth_token, uniqueness: true
  validates :password, length: { minimum: 6 }, on: :create
  validates :password, confirmation: true, on: :create
  validates :password_confirmation, presence: true, on: :create
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP, message: 'Only allows valid emails' },
                    uniqueness: true, email_top_level: true, if: :email_changed?
  validates_uniqueness_of :subscriber_account_id, allow_blank: true

  # Set associations
  has_and_belongs_to_many :applications
  has_many :user_sections
  has_many :sections, through: :user_sections

  has_one :subscription
  has_one :an_import, foreign_key: 'id', primary_key: 'username'
  has_one :mm_user, foreign_key: 'user_email', primary_key: 'email'
  has_many :user_videos, -> { uniq }, dependent: :destroy
  has_many :videos, through: :user_videos

  has_many :user_series, -> { uniq }, dependent: :destroy
  has_many :series, through: :user_series

  has_many :favorite_videos, -> { where favorite: true }, dependent: :destroy, class_name: 'UserVideo'
  has_many :favorites, through: :favorite_videos, source: :video

  has_many :queued_videos, -> { where enqueue: true }, dependent: :destroy, class_name: 'UserVideo'
  has_many :queued, through: :queued_videos, source: :video

  has_many :video_ratings, -> { where rating: 1..5 }, dependent: :destroy, class_name: 'UserVideo'
  has_many :ratings, through: :video_ratings, source: :video

  has_many :device_auths
  has_many :phones
  has_many :kids, -> { order(:dob) }
  has_many :addresses, -> { order(:address_type_id) }

  has_many :coupon_users, -> { uniq }, dependent: :destroy
  has_many :coupons, through: :coupon_users

  has_many :user_tickets, -> { uniq }, dependent: :destroy
  has_many :tickets, through: :user_tickets

  has_many :user_tags
  has_many :tags, through: :user_tags

  # Set scopes
  # default_scope { where(status: 'Active') }

  # def addresses
  #   Address.where(user_id: self.id).order(:address_type_id).as_json
  # end
  #
  # def phones
  #   Phone.where(user_id: self.id).as_json(only:[:id, :number, :carrier, :alias, :verification_code, :verified, :allows_messages, :created_at, :updated_at])
  # end
  #
  # def kids
  #   Kid.where(user_id: self.id).order(:dob).as_json
  # end

  def name
    "#{first_name} #{last_name}"
  end

  def tags
    tags = []
    user_tags.each do |ut|
      tag = {}
      tag_record = Tag.find(ut.tag_id)
      tag['id'] = tag_record.id
      tag['tag'] = tag_record.value
      tag['date_associated'] = ut.created_at
      admin_user = User.select('first_name, last_name').find(ut.admin_id)
      tag['added_by'] = "#{admin_user.first_name} #{admin_user.last_name}"
      tag['instance_id'] = ut.id
      tags << tag
    end
    tags
  end

  def devices
    device_auths.as_json(only: %i[id device_id device_type active created_at])
  end

  def security
    if admin?
      security = Section.all.as_json(only: [:name])
      # NOTE: Check access and role for each section
      security.each do |section|
        section[:has_access] = has_section_access?(section['name'])
        sect = sections.where('sections.name = ?', section['name']).first
        section[:role] = sect.roles.first.name if sect.present?
      end
    end
  end

  def billing_info
    ApplicationRecord.connected_to(role: :writing) do
      if billing_data.blank?
        update!(billing_data: (
          case subscription_source&.downcase
          when 'witb paypal'
            billing_info = {}
          when 'paypal'
            PaypalHelper::Agreement.retrieve(subscriber_account_id) if subscriber_account_id.present?
          when 'ios', 'amazon', 'roku'
            subscription_source
          else
            StripeHelper::Customer.retrieve(subscriber_account_id) if subscriber_account_id.present?
          end
        ).as_json)
      end
      billing_info = billing_data if billing_data.present?
      billing_info
    end
  end

  def subscription_info
    ApplicationRecord.connected_to(role: :writing) do
      if billing_info.present? && subscription_data.blank?
        update!(subscription_data: (
          case subscription_source&.downcase
          when 'paypal'
            billing_info['subscriptions']['data']
          when 'ios'
            AppleHelper.validate_receipt(self)
          when 'amazon'
            AmazonHelper.validate_receipt(self)
          else
            billing_info.respond_to?(:deleted) ? "Customer #{subscriber_account_id} Deleted" : StripeHelper::Subscription.retrieve_all(subscriber_account_id)
          end).as_json)
      elsif billing_info.blank?
        update!(subscription_data: [])
      end

      begin
        if (subscription_data.present? && !subscription_data.is_a?(String)) && !subscription_data.first['schedule'].present? && (subscription_source&.downcase == 'web' || subscription_source&.downcase == 'android')
        # Check if there is a subscription schedule
        schedule_events = Event.where(user_id: id).where("note ILIKE '%%Moving subscription%%'")
        schedule = nil
        schedule_events.each do |schedule_event|
          next unless schedule_event

          schedule_id = schedule_event.note.partition(':').last.strip
          next unless schedule_id.present?

          schedule = Stripe::SubscriptionSchedule.retrieve(schedule_id)
        end

        subscription_data.first['schedule'] = schedule

        if schedule.present? && (schedule.status == 'not_started' || schedule.status == 'active') && subscription_data.first['cancel_at_period_end'] == true
          # Add the schedule to be able to ensure if this is just cancelling due to price increase
          begin
            subscription_schedule = subscription_data.first['schedule']
            if subscription_schedule && subscription_schedule['canceled_at'].nil?
              subscription_data[0]['cancel_at'] = nil
              subscription_data[0]['cancel_at_period_end'] = false
              update!(subscription_data: subscription_data)
            end
          rescue Stripe::RateLimitError => e
            p 'Stripe request rate limited'
          end
        end
        end
      rescue StandardError => e
        p e
      end
    end

    subscription_info = subscription_data.present? ? subscription_data : []
    subscription_info
  end

  def charges
    return unless admin?

    StripeHelper::Charge.retrieve_all(customer: subscriber_account_id) if %w[web android].include? subscription_source&.downcase
  end

  def prorated_refund
    return unless admin?

    StripeHelper::Refund.get_proration(customer_id: subscriber_account_id, subscription_id: subscription_info.first['id']) if %w[web android].include? subscription_source&.downcase
  end

  # Define public methods
  def generate_authentication_token!
    # return if auth_token.present?
    self.auth_token = generate_auth_token
  end

  def has_section_access?(_section_to_check)
    # (self.sections.to_a.detect { |section| section.name == 'Global' || section.name == section_to_check }).present?
    true
  end

  def has_edit_access?(_section_to_check)
    # NOTE: This method encompasses the same logic as checking section access
    # So two checks do not need to be performed for edit rights in the policies
    # sect = self.sections.to_a.detect { |section| section.name == 'Global' || section.name == section_to_check }
    # roles = sect.roles if sect.present?

    # (roles.to_a.detect { |role| role.name == 'SuperAdmin' || role.name == 'Admin' || role.name == 'Editor' }).present?
    true
  end

  def is_section_admin?(_section_to_check)
    # NOTE: This method encompasses the same logic as checking section access
    # So two checks do not need to be performed for edit rights in the policies
    # sect = self.sections.to_a.detect { |section| section.name == 'Global' || section.name == section_to_check }
    # roles = sect.roles if sect.present?
    #
    # (roles.to_a.detect { |role| role.name == 'SuperAdmin' || role.name == 'Admin' }).present?
    true
  end

  def is_global_admin?
    # sect = self.sections.to_a.detect { |section| section.name == 'Global' }
    # roles = sect.roles if sect.present?
    #
    # (roles.to_a.detect { |role| role.name == 'SuperAdmin' }).present?
    true
  end

  # Define private methods

  private

  def sync_to_active_campaign
    ActiveCampaignHelper.sync_user(self)
  rescue StandardError => e
    p e
  end

  def check_password_length
    is_ok = password.nil? || password.empty? || password.length >= 6

    errors[:password] << 'Password is too short (minimum is 6 characters)' unless is_ok

    is_ok # The callback returns a Boolean value indicating success; if it fails, the save is blocked
  end

  def check_password_conf
    is_ok = password == password_confirmation

    errors[:password] << 'Password and confirmation do not match, please check values' unless is_ok

    is_ok # The callback returns a Boolean value indicating success; if it fails, the save is blocked
  end

  def generate_auth_token
    # SecureRandom.uuid.gsub(/\-/, '')
    # SecureRandom.base64(64).tr('+/=', '0aZ')
    # SecureRandom.base64(64).tr('+/=', '0aZ').strip.delete("\n")
    # SecureRandom.urlsafe_base64(64).tr('+/=', '0aZ').strip.delete("\n")
    # SecureRandom.urlsafe_base64(128).tr('+/=', '0aZ').strip.delete("\n")
    SecureRandom.urlsafe_base64(64).tr('+/=', '0aZ').tr('-', '').strip.delete("\n")
    # SecureRandom.urlsafe_base64(128).tr('+/=', '0aZ').tr('-', '').strip.delete("\n")
  end
end
