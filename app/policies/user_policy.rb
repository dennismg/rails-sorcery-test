class UserPolicy < ApplicationPolicy
    attr_reader :current_user, :model
  
    def initialize(current_user, model)
      @current_user = current_user
      @user = model
    end
  
    def index?
      @current_user.admin?
      @current_user.has_section_access?('Users')
    end
  
    def show?
      (@current_user.admin? and @current_user.has_section_access?('Users')) or @current_user == @user
    end
  
    def create?
      true
    end
  
    def update?
      (@current_user.admin? and @current_user.has_edit_access?('Users')) or @current_user == @user
    end
  
    def destroy?
      (@current_user.admin? and @current_user.has_edit_access?('Users')) or @current_user == @user
    end
  
    def set_active_episode?
      @current_user == @user
    end
  
    def list_favorites?
      @current_user == @user
    end
  
    def tag_in_intercom?
      true
      # @current_user == @user
    end
  
    class Scope < Scope
      def resolve
        scope
      end
    end
  end
  