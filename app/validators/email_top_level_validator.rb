class EmailTopLevelValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      unless valid_top_level_domain(value)
        record.errors.add(attribute, ': The email appears to be invalid. Please retype and/or use an alternative email address.')
      end
    end
  
    def valid_top_level_domain(value)
      tld = value.split('.').last
      tld_exclusions.exclude? tld
    end
  
    def tld_exclusions
      Setting.find_by(key: 'tld_exclusions').value.split(',')
    end
  end
  